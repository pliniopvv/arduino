#include <SoftwareSerial.h>

const int on_module_pin = 8;
SoftwareSerial gsm(2,3);

void setup() {
//  Serial.begin(2400);
    Serial.begin(9600);
//    Serial.begin(19200);
//  Serial.begin(57600);
//  Serial.begin(115200);

  gsm.begin(2400);
//    gsm.begin(9600);
//    gsm.begin(19200);
//  gsm.begin(57600);
//  gsm.begin(115200);
  pinMode(on_module_pin, OUTPUT);
  power_on();

  Serial.println("Modulo ligado...");
}

String cmd = "";
String received = "";

void loop() {
  Serial.print(".");
  if (Serial.available()>0) {
    cmd = "";
    byte testeDeCmd;
    while (Serial.available()>0) {
      testeDeCmd = Serial.read();
      if (!isCmd(testeDeCmd)) {
        cmd += (char)testeDeCmd;
        delay(20);
      } 
      else {
        cmd += "[";
        cmd += (char)testeDeCmd;
        cmd += "]";
      }
    }
    Serial.print("\nEnviando: ");
    Serial.println(cmd);
    gsm.println(cmd);
  }
  delay(100);
  if (gsm.available()>0) {
    delay(10);
//    received = "";
    Serial.print("\nRecebendo:\n");
    while(gsm.available()>0) {
//      received += (char)gsm.read();
      Serial.print((char)gsm.read());
      delay(20);
    }
    Serial.print("\n");
//    Serial.print("Recebido: ");
//    Serial.println(received);
  }
}

boolean isCmd(int vCmd) {
  int startCmds = 92; // -> \

  if (vCmd == startCmds) {
    switch (Serial.read()) {
    case 122: // -> z
      Serial.println("--> [enc_c] enviado");
      gsm.write(0x1a);
      gsm.print("\0");
      return true;
      break;
    case 110: // -> n
      gsm.print("\n");
      break;
    case 120: // -> x
      power_on();
      break;
    case 99: // -> c
      gsm.print("GET / HTTP/1.0 \n");
      delay(10);
      gsm.print("Host: www.ruidoterminal.com.br \n");
      delay(10);
      gsm.print("User-agent: Arduino \n\n");
      delay(10);
      break;
    case 118: // -> v
      break;
    case 98: // -> b
      break;
    case 97: // -> a
      break;
    case 100: // -> d
      break;
    }
    return true;
  }
  return false;
}

void power_on() {
  digitalWrite(on_module_pin,HIGH);
  delay(1200);
  digitalWrite(on_module_pin,LOW);
  delay(1000);
}























