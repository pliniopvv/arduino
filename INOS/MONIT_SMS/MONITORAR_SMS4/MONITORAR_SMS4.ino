#include <SoftwareSerial.h>

SoftwareSerial gsm(2, 3);
//
const int powerPin = 8;

void setup() {
  // put your setup code here, to run once:
  pinMode(powerPin, OUTPUT);
  Serial.begin(9600);
  gsm.begin(9600);

  power_on();
  delay(5000);

  gsm.println("AT+CMGF=1");
}

void loop() {
  if (Serial.available()>0) {
    String cmd = "";
    while (Serial.available()>0) {
      cmd += (char)Serial.read();
    }
    execCmd(cmd);
  }
  if (gsm.available()>0) {
    while (gsm.available()>0){
      Serial.print((char)gsm.read());
    } 
  }
  delay(10);
}



void execCmd(String cmd) {
  if (cmd.startsWith("RESTART")) { 
    power_on();
  } 
  else if (cmd.startsWith("PING")) {
    gsm.println("AT");
  } 
  else   if (cmd.startsWith("GET SMS")) {
    gsm.println("AT+CMGL=\"REC UNREAD\",0");
  } 
  if (cmd.startsWith("SEND SMS")) {
    //              SEND SMS [[92054014]][(olááááax)]
    //    gsm.println("AT+CSCS=\"8859-1\"");
    //    gsm.println("AT+CSCS=\"PCCP\"");
    //    gsm.println("AT+CSCS=\"HEX\"");
    //      gsm.println("AT+CSCS=\"UCS2\"");
    //      gsm.println("AT+CSCS=\"IRA\"");
    gsm.println("AT+CSCS=\"GSM\"");
    delay(1000);
    while (cmd.indexOf("..") != -1) {
      if (Serial.available() > 0) {
        cmd += (char)Serial.read();
      }
    }

    String numero = cmd.substring(cmd.indexOf("[[")+2,cmd.indexOf("]]"));
    String msg = cmd.substring(cmd.indexOf("[(")+2,cmd.indexOf(")]"));

    gsm.print("AT+CMGS=");
    gsm.write(byte(34));
    gsm.print(numero);
    gsm.write(byte(34));
    gsm.println();
    delay(500);
    gsm.print(msg);
    gsm.write(0x1a);
    gsm.println();
  }
}






























//
//
//
//
//      CFGS BÁSICAS ...
//
//
//
//


void power_on() {
  Serial.print("\n ORDER: START MODULE.\n");
  digitalWrite(powerPin, HIGH);
  delay(1000);
  digitalWrite(powerPin, LOW);
  delay(1000);
}



























