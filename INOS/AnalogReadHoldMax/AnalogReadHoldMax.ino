/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

int holdMax = 0;
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  
  if (sensorValue > holdMax) {
    holdMax = sensorValue;
  }
  Serial.println(holdMax);
  delay(1);        // delay in between reads for stability
}
