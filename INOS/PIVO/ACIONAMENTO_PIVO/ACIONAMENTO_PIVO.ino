#include "SIM900.h"
#include "sms.h"
#include "SoftwareSerial.h"
#include "sms.h"
#define start 9
#define rele 10
#define partida 11
const long tempoDeEspera = 20 * 60 * 60 * 1000;  // ( altere aqui para alterar o tempo de espera para verificação do status do contator, a unidade é millesegundos );
volatile long mlas = 0; // horário que o motor ligou (não mexa, o sistema irá utilizar esta variável);
volatile boolean verificado = false;
SMSGSM sms;


boolean started=false;
char smsbuffer[160];
char n[20];
int inc=10;
int  porta = 13;
//inserir o número telefonico
char number[]="+5516981002339";

void setup() 
{
  pinMode(partida,INPUT);
  pinMode(rele,OUTPUT);
  digitalWrite(rele,HIGH);
  pinMode(porta,OUTPUT);
  //Serial connection.
  Serial.begin(9600);
   pinMode(start, OUTPUT);
  //Mantem Modulo GSM desligado assim que iniciar o programa
  digitalWrite(start,HIGH);
  delay(1000); //tempo de 1 segundo para ligar o modulo GSM
  digitalWrite(start,LOW);
  delay(10000);
  Serial.println("GSM Shield Inicializado.");
  //Start configuration of shield with baudrate.
  if (gsm.begin(9600))
 
  {
    Serial.println("\nstatus=READY");
    if (sms.SendSMS(number, "PIVO AGUARDANDO COMANDO"))
    started=true;  
  }
  else Serial.println("\nstatus=IDLE");
  if(started){
    delsms();
  }

};

void loop() 
{
  int pos=0;
  //Serial.println("Loop");
  if(started){
    pos=sms.IsSMSPresent(SMS_ALL);
    if(pos){
      Serial.println("IsSMSPresent at pos ");
      Serial.println(pos); 
      sms.GetSMS(pos,n,smsbuffer,100);
      Serial.println(n);
      Serial.println(smsbuffer);
      if(!strcmp(smsbuffer,"B")){
        Serial.println("BLOQUEADO");
        digitalWrite(porta,LOW);
        delay(100);
        if (sms.SendSMS(number, "PIVO DESLIGADO"))
        Serial.println("\nSMS sent OK");
      }      
      if(!strcmp(smsbuffer,"L")){
        Serial.println("LIBERADO");
        digitalWrite(porta,HIGH);
		delay(100);
                mlas = millis(); ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  LINHA ADICIONADA
		if (sms.SendSMS(number, "PIVO ACIONADO")) {
        Serial.println("\nSMS sent OK");
        digitalRead(partida,LOW);
        delay(100);
        if (sms.SendSMS(number, "PIVO DESLIGADO"))
      }    
      delsms();
    }
  }
  
  
  /*
  
  Código que verificará se o tempo necessário passou, e então efetuará a verificação do sinal do contator.
  
  INÍCIO
  
  */
  
  if (Ndelay(mlas, tempoDeEspera)) {
    if (digitalRead(partida)) {
      sms.SendSMS(number, "O motor deu partida");
    } else {
      sms.SendSMS(number, "O motor NÃO deu partida");
    }
    
    mlas = millis(); // reinicia contagem para nova verificação...
  }
  
    /*
  
  Código que verificará se o tempo necessário passou, e então efetuará a verificação do sinal do contator.

  FIM
  
  */
  
}

void delsms(){
  Serial.println("delsms");
  for (int i=0; i<10; i++){  //do it max 10 times
    int pos=sms.IsSMSPresent(SMS_ALL);
    if (pos!=0){
      Serial.print("\nFind SMS at the pos ");
      Serial.println(pos); 
      if (sms.DeleteSMS(pos)==1){    
        Serial.print("\nDeleted SMS at the pos ");
        Serial.println(pos);      
      }
      else
      {
        Serial.print("\nCant del SMS at the pos ");
        Serial.println(pos);         
      }
    }
  }
}

boolean Ndelay(long startTime,long diff) {
  
  if ((millis() - startTime) >= diff) {
    return true;
  }
  return false;
  
}

