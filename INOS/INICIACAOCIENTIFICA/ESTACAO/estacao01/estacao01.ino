/*
Firmware feito para coleta de dados em SDCard.
 
 */









#include <SD.h> 
//#define debug // como debugar ainda nÃ£o definido.
#define onpc












//CONFIGURAÇÕES DO HARDWARE;
const int pA = 9; // pA = Pin Alert;
const int buttonPin = 8;
const int scSD = 10; // Select Cable SD;

//CONFIGURAÇÕES PREFERENCIAIS;
const char* nomeArquivo = "dados.csv";
const int timeCollect =  15    *1000     *60; // *1000 (1 segundo) *60 minuto (1 minuto)












void setup() {
#ifdef onpc
  Serial.begin(9600);
  Serial.println("Iniciando Sistema");
#endif

  pinMode(scSD, OUTPUT);
  pinMode(pA, OUTPUT);
  pinMode(buttonPin, INPUT);
  
  
  startSD();
}

void loop() {
  //    piscarLed(10); // OK
  /* OK
   fadeIn();
   fadeOut();
   */


  pressButton();

  saveData("primeiraKey",500);
  
  delay(1000);
  
  while(true) {
    fadeIn();
    piscarLed(5);
    fadeOut();
  }
}
























// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
void piscarLed(int nVezes) {
  for (int i = 0; i < nVezes;i++) {
    digitalWrite(pA, HIGH);
    delay(100);
    digitalWrite(pA, LOW);
    delay(100);
  }
}
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
void fadeIn() {
  for (int i = 0;i<255;i++) {
    analogWrite(pA, i);
    delay(1);
#ifdef onpc
    Serial.print("Led FadeValue: ");
    Serial.println(i);
#endif
  }
}
void fadeOut() {
  for (int i = 255;i>=0;i--) {
    analogWrite(pA, i);
    delay(1);
#ifdef onpc
    Serial.print("Led FadeValue: ");
    Serial.println(i);
#endif
  }
}
















//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
void startSD() {
  if (!SD.begin(scSD)) {
    while (true) {
      fadeIn();
      fadeOut();
#ifdef onpc
      Serial.println("Falha em iniciar SD Card.");
#endif
    }
    piscarLed(1);
  }
}
void saveData(char* key, int value) {
  File dataFile = SD.open(nomeArquivo, FILE_WRITE);

  if (dataFile) {
    dataFile.print(key);
    dataFile.print(";");
    dataFile.println(value);
    dataFile.close();
#ifdef onpc
    Serial.print(key);
    Serial.print(";");
    Serial.println(value);
#endif
    piscarLed(1);
  } 
  else {
    while (true) {
      fadeIn();
      piscarLed(2);
      fadeOut();
    }
  }
}





















//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
void pressButton() {
  boolean  buttonState = true;
  while (buttonState) {
    piscarLed(1);
    boolean buttonRead = digitalRead(buttonPin);

#ifdef onpc
    Serial.print("Leitura do botão: ");
    Serial.println(buttonRead);
#endif

    if (buttonRead == LOW) {
      fadeIn();
      fadeOut();
      piscarLed(2);
      buttonState = false;
    }
  }  
}








