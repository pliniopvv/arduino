#define SensorRPM A0


void setup() {
  Serial.begin(9600);
  Serial.println("System ligado.");
}

void loop() {
  Serial.print(getRPM(),10);
  Serial.println(" Minutos");
  delay(1);
}

volatile int sensorValue = 50;
volatile int count = 0;
volatile boolean p1 = false;
volatile boolean p2 = false;
float getRPM() {
  float retorno = -1;
  p1 = false;
  p2 = false;
  int t1 = 0;
  int t2 = 0;
  while (p2 == false) {
    sensorValue = analogRead(SensorRPM);

#ifdef debugRPM
    Serial.println(sensorValue);
    Serial.println(p1);
    Serial.println(p2);
    delay(300);
    Serial.println(t1);
    Serial.println(t2);
#endif

    if (sensorValue > 1000) {
      if (p1 == false) {
        p1 = true;
        t1 = millis();
#ifdef debugRPM
        Serial.println(t1);
#endif
      }
    }      
    else if ((p2 == false) && (p1 == true) && (sensorValue < 1000)) {
      p2 = true;
      t2 = millis();
      retorno = diffMinutes(t1,t2);

#ifdef debugRPM
      Serial.println(t2);
#endif
    }
    if (retorno > 0) {
      return retorno;
    }
  }
}

float diffMinutes(int t1,int t2) {

#ifdef debugDiffMinutes
  Serial.print("t1: ");
  Serial.print(t1);
  Serial.print(" ");
  Serial.print("t2: ");
  Serial.println(t2);
#endif

  float rmS = t2 - t1;

#ifdef debugDiffMinutes
  Serial.print("rmS: ");
  Serial.println(rmS);
#endif

  float rM = (float) rmS / 60000.0; // 1 milisegundo equivale a 1/60000 minutos;
  return rM;
}












