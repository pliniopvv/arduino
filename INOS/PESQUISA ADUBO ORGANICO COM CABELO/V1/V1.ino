// PROGRAMA PROJETO SENSORES DE TEMPERATURA PARA ADUBO ORGANICO
#define debug


#include <SD.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include "RTClib.h"

#define WIRE_SENSORES 8
#define WIRE_SD 7

RTC_Millis relogio;

OneWire oneWire(WIRE_SENSORES);

DallasTemperature sensores(&oneWire);
DeviceAddress sensor1;
DeviceAddress sensor2;
DeviceAddress sensor3;
DeviceAddress sensor4;


volatile int total_sensores = 0;
volatile int id_sensor = 0;

void setup() {
  // put your setup code here, to run once:
#ifdef debug
  Serial.begin(9600);
  Serial.println("Iniciando Sistema ..");
#endif

  relogio.begin(DateTime(F(__DATE__), F(__TIME__)));

#ifdef debug
  Serial.println(":: Módulo do Relógio iniciado.");
#endif


  pinMode(WIRE_SD, OUTPUT);
  if (!SD.begin(WIRE_SD)) {
    // CARTÃO SD NÃO FUNCIONANDO
#ifdef debug
    Serial.println(":: Cartão SD não está funcionando.");
#endif
    while (true) {
      // XXXX adicionar sistema de alerta para problemas no cartão.
    }
  }
#ifdef debug
  Serial.println(":: Módulo SD iniciado.");
#endif

#ifdef debug
  Serial.println(":: Inicializando Sensores.");
#endif

  sensores.begin();
  if (!sensores.getAddress(sensor1, 0))
    Serial.println("Sensor 01 nao encontrado !");
  if (!sensores.getAddress(sensor2, 1))
    Serial.println("Sensor 02 nao encontrado !"); 
  if (!sensores.getAddress(sensor3, 2))
    Serial.println("Sensores 03 nao encontrado !"); 
  if (!sensores.getAddress(sensor4, 3))
    Serial.println("Sensores 04 nao encontrado !"); 

  total_sensores = sensores.getDeviceCount();
  if (total_sensores == 0) {
#ifdef debug
    Serial.println(":: Nenhum sensor encontrado.");
#endif 
    while (true) {
      //  XXXX adicionar sistema de alerta para problemas com conexão dos sensores.
    }
  }

#ifdef debug
  Serial.print(":: Sensores encontrados -> ");
  Serial.println(total_sensores, DEC);
#endif
}

void loop() {
  String data = "";
  sensores.requestTemperatures();
  //Sensor
  data += hora_agora();
  data += ";";
  data += pega_cod(sensor1); // XXXX fazer código
  data += ";";
  float temp = sensores.getTempC(sensor1);
  char t[8];
  dtostrf(temp,4,4,t);
  data += t;
  write_sd(data);
  data = "";
  //Sensor
  data += pega_cod(sensor2); // XXXX fazer código
  data += ";";
  temp = sensores.getTempC(sensor2);
  t[8];
  dtostrf(temp,4,4,t);
  data += t;
  write_sd(data);
  data = "";
  //Sensor
  data += pega_cod(sensor3); // XXXX fazer código
  data += ";";
  temp = sensores.getTempC(sensor3);
  t[8];
  dtostrf(temp,4,4,t);
  data += t;
  write_sd(data);
  data = "";
  //Sensor
  data += pega_cod(sensor4); // XXXX fazer código
  data += ";";
  temp = sensores.getTempC(sensor4);
  t[8];
  dtostrf(temp,4,4,t);
  data += t;
  data += "\n";
  write_sd(data);
  delay(1000);
}

String pega_cod(DeviceAddress deviceAddress) {
  String data = "";
  for (uint8_t i = 0; i < 8; i++)  {
    // Adiciona zeros se necessário
    if (deviceAddress[i] < 16) { 
      data += "0"; 
    }
    data += String(deviceAddress[i], HEX);
  }
  return data;
}

String hora_agora() {
  String data = "";
  DateTime agora = relogio.now();
  data += String(agora.day(), DEC);
  data += "-";
  data += String(agora.month(), DEC);
  data += "-";
  data += String(agora.year(), DEC);
  data += " ";
  data += String(agora.hour() , DEC);
  data += "h";
  data += String(agora.minute() , DEC);
  data += "min";
  data += String(agora.second() , DEC);
  data += "s";
  return data;
}

void write_sd(String data) {
  File dataFile = SD.open("registros.csv", FILE_WRITE);
  if (dataFile) {
    dataFile.print(data);
    dataFile.close();
  } 
  else {
#ifdef debug
    Serial.println(":: Cartão apresentou problemas.");
#endif
    // XXXX avisar que o cartão SD está com problemas.
  }

#ifdef debug
  Serial.print(data);
#endif
}



