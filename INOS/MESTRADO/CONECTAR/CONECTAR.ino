#include <SoftwareSerial.h>

// ------------------------                       CONFIGURAÇÃO DO MÓDULO.
SoftwareSerial gsm(2,3); // RX, TX.
const int on_module_pin = 8;

#define debug on
//#define debug_module on
//#define debug_conection on

void setup() {
  pinMode(on_module_pin, OUTPUT);

  Serial.begin(9600);

























#ifdef debug
  Serial.println("Ligando modulo ...");
#endif

  power_on(); // ---------------         LIGANDO MÓDULO!































#ifdef debug
  Serial.println("Ligando SoftwareSerial do modulo...");
#endif

  gsm.begin(2400);




































#ifdef debug_module
  Serial.println("--- init");

  gsm.println("AT");
  Serial.println("AT");
  delay(700);

  Serial.print("\nData recebida: ");
  Serial.println(gsm.available());

  Serial.println("-");
  while(gsm.available() > 0) {
    Serial.print((char)gsm.read()); 
  }
  Serial.println("-");

  Serial.println("--- end");
#endif







  //  delay(5000);
  //  gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
  //  delay(1000);
  //  gsm.println("AT+CIICR");
  //  delay(1000);
  //  gsm.println("AT+CIFSR");



#ifdef debug_conection
  Serial.println("Iniciando conexão com a internet ...");
  //  delay(5000);

  Serial.print("\nData recebida: ");
  Serial.println(gsm.available());

  while(gsm.available() > 0) {
    Serial.print((char)gsm.read());
  }

  Serial.println("--- end");
#endif
}

void loop() {

  read_sensor();

  response_analiser();  
  send_request_module();

}

























//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO

void power_on() {
  digitalWrite(on_module_pin,HIGH);
  delay(1200);
  digitalWrite(on_module_pin,LOW);
  delay(1000);
}

//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO

int request_state = 0;
long old_module_request_time = 0;
boolean response_analised = true;    // APENAS ENVIA REQUEST SE A RESPOSTA TIVER SIDO ANALISADA.


void send_request_module() {
  if (newDelay(old_module_request_time, 5000) || response_analised == true) {
    switch (request_state) {
    case 0:
      gsm.println("AT");
      Serial.print("Verificando status do modulo ::\n");

      finish_request_module();
      break;
    case 1:
      gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
      Serial.print("Configurando modulo ::\n");

      finish_request_module();

      break;
    case 2:
      gsm.println("AT+CIICR");
      Serial.print("Iniciando processo de conexao no modulo ::\n");

      finish_request_module();

      break;
    case 3:  
      gsm.println("AT+CIFSR");
      Serial.print("Requisitando IP ::\n");

      finish_request_module();
      break;
    case 4:
      gsm.println("AT+CIPSTART=\"TCP\",\"www.meioamb.eng.br\",80");
      Serial.print("Requisitando conexao com o servidor ::\n");

      finish_request_module();

      break;
    case 5:
      gsm.println("AT+CIPSEND");
      Serial.print("Iniciando envio de dados ::\n");

      finish_request_module();

      break;
    case 6:
      gsm.print("GET /m/save.php?temp=");
      gsm.print(getData());                        //       OBTEM OS DADOS A SEREM ENVIADOS ...
      gsm.print(" HTTP/1.0 \n");
      gsm.print("Host: www.meioamb.eng.br \n");
      gsm.print("User-agent: Arduino");
      gsm.print("\n\n");
      gsm.write(0x1a);
      gsm.print("\0");

      Serial.print("Enviando dados ::\n");
      finish_request_module();

      break;
    }
  }
}

boolean newDelay(long time, int diff) {
  if ((millis() - time) > diff) {
    return true;
  }
  return false;
}




//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE

int response_state = 0;
long old_response_analised_time = 0;  //  TEMPO DA ULTIMA RESPOSTA ANALISADA (CONFIRMADA).

void response_analiser() {
  String received = "";
  if ((gsm.available()>0)) {
    received = "";
//    delay(1000);
//    boolean continua = true;
//    Serial.print("::::::::: buffer: ");
//    Serial.println(gsm.available());
//    while (continua)
    while(gsm.available()>0) {
      received += (char)gsm.read();
      delay(50);
//      if (!(gsm.available()>0)) {
//        continua = false;
//      }
    }
    Serial.print("Recebido: ");
    Serial.println(received);

//    Serial.print("Analisando Response ::\n");

    int response;
    switch (response_state) {
    case 0:
      response = response_analise(received,"OK","NORMAL POWER DOWN");
      if (response == 1) {
        Serial.print("RA: Modulo ligado e respondendo ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Modulo desligado ou nao responsendo ::\n");
      }
      break;
    case 1:
      response = response_analise(received,"OK","ERROR");
      Serial.print("==> ");
      Serial.println(response);
      if (response == 1) {
        Serial.print("RA: Configuracao aceita ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Configuracao negada ::\n");
      }
      break;
    case 2:
      response = response_analise(received,"OK","ERROR");
      if (response == 1) {
        Serial.print("RA: Requisicao de conexao aceita ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Requisicao de conexao negada ::\n");
      }
      break;
    case 3:
      response = response_analise(received,"OK","ERROR");
      if (response == 1) {
        Serial.print("RA: IP Confirmado com sucesso ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: IP Nao Confirmado ::\n");
      }
      break;
    case 4:
      response = response_analise(received,"CONNECT OK","ERROR");
      if (response == 1) {
        Serial.print("RA: Conexao estabelecida com a internet ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Conexao recusada com o servidor ::\n");
      }
      break;
    case 5:
      response = response_analise(received,">","ERROR");
      if (response == 1) {
        Serial.print("RA: Modulo aceitando envio de dados ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Envio de dados negado ::\n");
      }
      break;
    case 6:
      response = response_analise(received,"SEND OK","ERROR");
      if (response == 1) {
        Serial.print("RA: Dados enviados com sucesso, reiniciando envio. ::\n");
      } 
      else if (response == 0) {
        Serial.print("RA: Dados nao enviados. ::\n");
      }
      break;
    }
  }
}

int response_analise(String received, String TRUE, String FALSE) {
  if (newDelay(old_response_analised_time, 500)) {
    if (received.indexOf(FALSE) > -1) {
      old_response_analised_time = millis();
      back_request();
      return 0;
    } 
    else if (received.indexOf(TRUE) > -1) {
      old_response_analised_time = millis();
      next_request();
      return 1;
    } 
    else {
      back_request();
      return 3;  // TEMPO ESTOURADO ::
    }
  }
  return 2;  // AGUARDANDO O TEMPO ::
}


// VÍNCULO ENTRE O REQUEST E O RESPONSE ::

void finish_request_module() {
  response_state = request_state;
  response_analised = false;
  old_module_request_time = millis();
  Serial.print("Module Request State -> ");
  Serial.println(request_state);
//  Serial.print("Aguardando analise ::\n");
}

void back_request() {
  if (request_state > 0) {
    request_state--;
  }
  response_analised = true;
}
void next_request() {
  if (request_state < 6){
    request_state++;
  }
  response_analised = true;
}






//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR

int sensor_data = 0;
void read_sensor() {
  sensor_data = analogRead(A0);
}
float getData() {
  return sensor_data * (5.0 / 1023.0) * 100;
}
























