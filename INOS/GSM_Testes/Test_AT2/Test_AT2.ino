#define debug

const int pinPower = 8;

// DEFINITIONS

const int buffGSMSize = 50;

#include <SoftwareSerial.h>

SoftwareSerial gsm(2,3);

void setup() {

  Serial.begin(9600);
  gsm.begin(2400);

#ifdef debug
  Serial.println("");
#endif

#ifdef debug
  Serial.println("Serial e SoftwareSerial iniciados ...");
#endif

#ifdef debug
  Serial.println("Inicializando pinos ...");
#endif

  //INICIALIZA DEFINIÇÕES
  pinMode(pinPower, OUTPUT);

  //SETUPS
  power_on();
}

boolean power_on() {

  digitalWrite(pinPower, HIGH);
  delay(1400);
  digitalWrite(pinPower, LOW);

  gsm.println("AT");
  delay(1000);
}

boolean SERIALtoGSM() {
#ifdef debug
  Serial.print(" ......... :: gsm.available() : ");
  Serial.println(Serial.available());
  Serial.println(" ......... :: Recebendo dados SERIAL");
#endif

  char buff;
  while (Serial.available() > 0) {
    buff = (char)Serial.read();
    gsm.print(buff);

#ifdef debug
    Serial.println(buff);
#endif
  }

#ifdef debug
  Serial.println(" ......... :: Fim dos dados SERIAL");
#endif

  return true;
}

boolean GSMtoSERIAL() {
#ifdef debug
  Serial.print(" ......... :: gsm.available() : ");
  Serial.println(gsm.available());
  Serial.println(" ......... :: Recebendo dados GSM");
#endif

  char buff;

  while (gsm.available() > 0) {
    buff = (char)gsm.read();
    Serial.print(buff);
  }
  Serial.println("");

#ifdef debug
  Serial.println(" ......... :: Fim dos dados GSM");
#endif

  return true;
}

void loop() {
  
  SERIALtoGSM();
  
  delay(1000);
  
  GSMtoSERIAL();

  delay(20000);
  
}






