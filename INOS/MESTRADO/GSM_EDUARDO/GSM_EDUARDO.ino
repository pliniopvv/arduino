#include <SoftwareSerial.h>

const int on_module_pin = 8;
SoftwareSerial gsm(2, 3);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  gsm.begin(2400);

  pinMode(on_module_pin, OUTPUT);
  Serial.println("Fim do arranque, iniciando loop.");
}

boolean modulo_on = true;
void loop() {
  Serial.println("LS [");
  limpabuffer();

  Serial.println("Verificando status do modulo...");
  while (!verifica_modulo()) {
    Serial.println("Modulo desligado, inicializando modulo.");
    power_on();
    delay(2000);
  }
  Serial.println("Modulo ligado.");
  while (modulo_on) {
    Serial.println("Inicializando serviço web.");
    gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
    delay(1000);
    gsm.println("AT+CIICR");
    delay(1000);
    gsm.println("AT+CIFSR");
    delay(1000);
    limpabuffer();
    while (modulo_on) {
      //      ler_sensores();
      Serial.println("Nova Leitura (");
      gsm.println("AT+CIPSTART=\"TCP\",\"www.ruidoterminal.com.br\",80");
      delay(1000);
      gsm.println("AT+CIPSEND");
      delay(1000);
      float dba = getDecib(); //       OBTEM OS DADOS A SEREM ENVIADOS ...
      float temp = getTemp();
      //      gsm.print("GET /insert.php?token=2526272625&db=10.5&temp=66.6");
      delay(100);
      gsm.print("GET /insert.php?token=2526272625&db=");
      gsm.print(dba);
      gsm.print("&temp=");
      gsm.print(temp);
      gsm.println(" HTTP/1.0");
      gsm.println("Host: www.ruidoterminal.com.br");
      gsm.print("User-agent: Arduino");
      gsm.println("\n");
      delay(100);
      gsm.write(0x1a);
      gsm.println("\0");
      Serial.print("db(A): ");
      Serial.println(dba);
      Serial.print("Temp: ");
      Serial.println(temp);
      Serial.println(")");
      limpabuffer();
      delay(5000);
      delay(1000);
    }
  }

  Serial.println("]");
  delay(1000);
}

boolean verifica_modulo() {
  limpabuffer();


  gsm.println("AT");
  char retorno[10];
  int cr = 0;

  //LIMPANDO ARRAY;
  for ( int i = 0; i < sizeof(retorno) - 1; i++ ) {
    retorno[i] = (char)0;
  }

  delay(700);
  for (int i = 0; i < 500; i++) {
    while (gsm.available() > 0) {
      retorno[cr] = (char)gsm.read();
      cr++;
      delay(20);
    }
    delay(10);
  }
  // OUTPUT O RECEBIDO:
  Serial.println("Recebido da Verificacao (");
  Serial.println(retorno);
  Serial.println(")");

  Serial.println("Analise retorno (");
  for (int i = 0; i < sizeof(retorno) - 1; i++) {
    char c = retorno[i];
    if (c == 'O') {
      char p = retorno[i + 1];
      if (p == 'K') {
        Serial.println("TRUE");
        Serial.println(")");
        return true;
      }
    }
  }
  Serial.println("FALSE");
  Serial.println(")");
  return false;
}

void power_on() {
  digitalWrite(on_module_pin, HIGH);
  delay(1200);
  digitalWrite(on_module_pin, LOW);
  delay(1000);
}

char buff[300];
void limpabuffer() {
  Serial.print("limpando buffer: ");
  Serial.print(gsm.available());
  Serial.println(" (");
  int cr = 0;

  //LIMPANDO ARRAY;
  for ( int i = 0; i < sizeof(buff) - 1;  i++ ) {
    buff[i] = (char)0;
  }

  for (int i = 0; i < 299; i++) {
    while (gsm.available() > 0) {
      buff[cr] = (char)gsm.read();
      cr++;
      delay(25);
    }
    delay(1);
  }
  Serial.println(buff);
  Serial.println(")");
}


float getDecib() {
  return analogRead(A0) * (5.0 / 1023.0) * 100;
}
float getTemp() {
  return analogRead(A1) * (5.0 / 1023.0) * 100;
}
