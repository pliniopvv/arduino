/*
Firmware feito para coleta de dados em SDCard.
 
 */









#include <SD.h>
//#define onpc
//#define debug // como debugar ainda nÃ£o definido.













//CONFIGURAÇÕES DO HARDWARE;
const int pA = 9; // pA = Pin Alert;
const int buttonPin = 8;
const int scSD = 10; // Select Cable SD;

//CONFIGURAÇÕES PREFERENCIAIS;
const char* nomeArquivo = "dados.csv";
const int timeCollect =  1000; // *1000 (1 segundo) *60 minuto (1 minuto)












void setup() {
#ifdef onpc
  Serial.begin(9600);
  Serial.println("Iniciando Sistema");
#endif

  pinMode(scSD, OUTPUT);
  pinMode(pA, OUTPUT);
  pinMode(buttonPin, INPUT);

  startSD();
  pressButton();
}

void loop() {
  int wTime = wait(timeCollect);

  int sensor = analogRead(A0);
  sensor = sensor * (5.0 / 1023.0) * 100;
  saveData("A0", wTime, sensor);
}





// FUNÇÕES DO SOFTWARE;
// FUNÇÕES DO SOFTWARE;
// FUNÇÕES DO SOFTWARE;
// FUNÇÕES DO SOFTWARE;
// FUNÇÕES DO SOFTWARE;
int wait(int Milleseconds) {
  int init = millis();
  int inst = millis();
  while ((inst - init) <= Milleseconds) {
    inst = millis();
  }
  return (inst - init);
}



















// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
// FUNÇÕES DO LED;
void piscarLed(int nVezes) {
  for (int i = 0; i < nVezes;i++) {
    digitalWrite(pA, HIGH);
    delay(100);
    digitalWrite(pA, LOW);
    delay(100);
  }
}
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
//FUNÇÕES PARA CONTROLE DO LED;
void fadeIn() {
  Serial.print("Led FadeIn! ");
  for (int i = 0;i<255;i++) {
    analogWrite(pA, i);
    delay(1);
#ifdef debug
    Serial.println(i);
#endif
  }
}
void fadeOut() {
  Serial.print("Led FadeOut! ");
  for (int i = 255;i>=0;i--) {
    analogWrite(pA, i);
    delay(1);
#ifdef debug
    Serial.println(i);
#endif
  }
}
















//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
//FUNÇÕES PARA CONTROLE DO CARTÃO SD;
void startSD() {
  if (!SD.begin(scSD)) {
    while (true) {
      fadeIn();
      fadeOut();
#ifdef debug
      Serial.println("Falha em iniciar SD Card.");
#endif
    }
    piscarLed(1);
  }
}
void saveData(char* key, int time, int value) {
  File dataFile = SD.open(nomeArquivo, FILE_WRITE);

  if (dataFile) {
    dataFile.print(key);
    dataFile.print(";");
    dataFile.println(value);
    dataFile.close();
#ifdef debug
    Serial.print(key);
    Serial.print(";");
    Serial.print(time);
    Serial.print(";");
    Serial.println(value);
#endif
    piscarLed(1);
  } 
  else {
    while (true) {
      fadeIn();
      piscarLed(2);
      fadeOut();
    }
  }
}





















//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
//FUNÇÕES PARA BUTTON;
void pressButton() {
  boolean  buttonState = true;
  while (buttonState) {
    piscarLed(1);
    boolean buttonRead = digitalRead(buttonPin);

#ifdef debug
    Serial.print("Leitura do botão: ");
    Serial.println(buttonRead);
#endif

    if (buttonRead == LOW) {
      fadeIn();
      fadeOut();
      piscarLed(2);
      buttonState = false;
    }
  }  
}











