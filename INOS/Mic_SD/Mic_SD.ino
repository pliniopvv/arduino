/*
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** CLK - pin 13
 ** MISO - pin 12
 ** CS - pin 4 CHANGED FOR 10
 */

#include <SD.h>

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
#define startPin 8
#define micPin A0
#define chipSelect 10
//#define debug
#define ledInfo 9
//#define stopTime 10 //em minutos

void fadeLed() {
}

void startSD() {
  pinMode(chipSelect, OUTPUT); // Para o SD;
  if (!SD.begin(chipSelect)) {
#ifdef debug
    Serial.println("Falha em iniciar");
#endif
  }
  else {
#ifdef debug
    Serial.println("Cartao inicializado.");
#endif
  } 
}


void startButton() {
  pinMode(5,INPUT);
  pinMode(ledInfo, OUTPUT);

#ifdef debug
  Serial.println("Favor precionar o boto para iniciar ...");
#endif

  boolean start = false;
  boolean ledState = true;
  int timeLed = millis();
  while (!start) {
    if ((millis() - timeLed) > 100) {
      if (ledState == true) {
        digitalWrite(ledInfo, HIGH);
      } 
      else {
        digitalWrite(ledInfo, LOW);
      }
      ledState = !ledState;
      timeLed = millis();
    }
    if (digitalRead(startPin) == LOW) {
      start = true;
#ifdef debug
      Serial.println("Iniciando captaçao ...");
#endif
      digitalWrite(ledInfo, LOW);
    }
  } 
}

void errorSDAlert(){
  while(true) {
    int brilho = 0;
    for (brilho = 0; brilho < 256; brilho++) {
      analogWrite(ledInfo, brilho);
      delay(1);
    }
    for (brilho = 255; brilho > 0; brilho--) {
      analogWrite(ledInfo, brilho);
      delay(1);
    }
  } 
}

boolean checkFormatFile() {
  if (!SD.exists("mic.csv")) { // Se mic.csv nao existir ...
#ifdef debug
    Serial.println("Criando arquivo.");
#endif
    SD.open("mic.csv", FILE_WRITE); // Cria o arquivo como escrita.
  }
  if (SD.exists("mic.csv")) { // Se o arquivo mic.csv existir ...
#ifdef debug
    Serial.println("Arquvo criado.");
#endif
    File dataFile = SD.open("mic.csv", FILE_WRITE); // Abre o arquivo mic.csv
    dataFile.println(String("diff Tempo (em milisegundos);Dados")); // Cria o cabeçalho da planilha.
    dataFile.close();
  } 
  else { // Se o arquivo mic.csv nao existir...
    while (true) {
#ifdef debug
      Serial.println("Falha em criar/abrir o arquivo. \nParando systema.");
#endif
      errorSDAlert();
    }
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Firmware 1.0.0");

#ifdef chipSelect
  startSD();
#endif

#ifdef startPin
  startButton();
#endif

  checkFormatFile();
}

float getMic() {
  int sensor = 0;
  //  while (sensor < 500) {
  sensor = analogRead(micPin); 
  //  }

  float mic = sensor*1.0;
  return mic;
}


volatile int initTime = -1;
void checkTime() {
#ifdef stopTime
  if (initTime < 0) {
    initTime = millis();
  }
  int nowTime = millis();
  int diff = initTime - nowTime;
  if (stopTime*1000 >= diff) {
#ifdef debug
    Serial.println("Tempo esgotado.");
#endif
    while (true);
  }

#endif
}

volatile long ultimoSalvo = 0;
void saveData(String data)
{
  File dataFile = SD.open("mic.csv", FILE_WRITE);
  if (!SD.exists("mic.csv")) {
    //#ifdef debug
    //    Serial.println("O arquivo nao foi criado.");
    //#endif
    //checkFormatFile();
    errorSDAlert();
  }
  if (dataFile) {
    int agora = millis();
    int diff = agora - ultimoSalvo;
    ultimoSalvo = agora;
    dataFile.println(String(diff) + ";" + data);
    dataFile.close();
//    Serial.println(diff);
  }
}

void loop()
{
  //  float mic = getMic();
  int mic = getBufferedMic(990);
  char data[5];
  dtostrf(mic,2,2,data);

#ifdef debug
  Serial.print("Millis : ");
  Serial.print(millis());
  Serial.print(" Data : ");
  Serial.println(data);
#endif

#ifdef stopTime
  checkTime();
#endif

  saveData(data);
  //  delay(10);
}

int getBufferedMic(int buff) {
  int agora = millis();
  int comeco = agora;
  int oldValue = analogRead(micPin);
  while ((agora - comeco) < buff) {
    int newValue = analogRead(micPin);
    if (newValue > oldValue) {
      oldValue = newValue;
    }
    agora = millis();  
  }
  return oldValue;
}


