//#define debugRPM // Descomentar para debugar o codigo.
#define SensorRPM A0
#define sensorMidium 1000 // Valor de virada do sensor de infravermelho;

void setup() {
  Serial.begin(9600);
  Serial.println("System ligado.");
}

void loop() {
  Serial.print(getRPM(),10);
  Serial.println(" Minutos");
  delay(1);
}

volatile int sensorValue = 50;
volatile int count = 0;
volatile boolean p1 = false;
volatile boolean p2 = false;
volatile boolean r1 = false;
volatile boolean r2 = false;
float getRPM() {
  float retorno = -1;
  p1 = false;
  r1 = false;
  p2 = false;
  r2 = false;
  int tr1 = 0;
  int tr2 = 0;

#ifdef debugRPM
  Serial.println("Tacometro iniciado, aguardando movimentos no sensor.");
#endif

  while (r2 == false) {
    int sensorValue = analogRead(SensorRPM);
    if (sensorValue > sensorMidium) {
      if ((p1 == false) && (r1 == false)) { // Verifica se já possui primeira entrada registrada.
        p1 = true; // Registra a primeira entrada.

#ifdef debugRPM
        Serial.println("Primeira entrada registrada.");
#endif
      } 
      else if ((p2 == false) && (r2 == false) && (p1 == true) && (r1 == true)) { // Verifica se já possui a primeira entrada e a primeira saída.
        p2 = true; // Registra a segunda entrada.

#ifdef debugRPM
        Serial.println("Segunda entrada registrada.");
#endif
      }
    } 
    else if (sensorValue < sensorMidium) {
      if ((p1 == true) && (r1 == false) && (p2 == false)) { // Verifica a primeira entrada e se já possui a primeira saída registrada.
        r1 = true; // Registra a primeira saída.
        tr1 = millis(); // Registra o tempo da primeira saída.

#ifdef debugRPM
        Serial.println("Primeira saida registrada.");
#endif
      } 
      else if ((p2 == true) && (r2 == false) && (r1 == true) && (p1 == true)) { // Verifica a segunda entrada.
        r2 = true; // Registra a segunda saída.
        tr2 = millis(); // Registra o tempo da segunda saída.

#ifdef debugRPM
        Serial.println("Segunda saida registrada.");
#endif
      }
    }
  }

#ifdef debugRPM
  Serial.println("Calculando tempo da Volta: ");
#endif
  return diffMinutes(tr1, tr2);
}

float diffMinutes(int tr1,int tr2) {

#ifdef debugDiffMinutes
  Serial.print("Tempo da primeira saída: ");
  Serial.print(tr1);
  Serial.print(" ");
  Serial.print("Tempo da segunda saída: ");
  Serial.println(tr2);
#endif

  float rmS = tr2 - tr1;

#ifdef debugDiffMinutes
  Serial.print("rmS: ");
  Serial.println(rmS);
#endif

  float rM = (float) rmS / 60000.0; // 1 milisegundo equivale a 1/60000 minutos;
  return rM;
}





















