#include <SoftwareSerial.h>

int pushButton = 2;
SoftwareSerial soft(6,7);

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  soft.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
    String f = "";
    if (soft.available() > 0) {
        char c = (char) soft.read();
        f += c;
    }
    Serial.println(f);
}




