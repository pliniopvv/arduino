#include <SoftwareSerial.h>

// ------------------------                       CONFIGURAÇÃO DO MÓDULO.
SoftwareSerial gsm(2,3); // RX, TX.
const int on_module_pin = 8;

#define debug on
//#define debug_module on
//#define debug_conection on

void setup() {
  pinMode(on_module_pin, OUTPUT);

  Serial.begin(9600);

























#ifdef debug
  Serial.println("Ligando modulo ...");
#endif

  power_on(); // ---------------         LIGANDO MÓDULO!































#ifdef debug
  Serial.println("Ligando SoftwareSerial do modulo...");
#endif

  gsm.begin(2400);




































#ifdef debug_module
  Serial.println("--- init");

  gsm.println("AT");
  Serial.println("AT");
  delay(700);

  Serial.print("\nData recebida: ");
  Serial.println(gsm.available());

  Serial.println("-");
  while(gsm.available() > 0) {
    Serial.print((char)gsm.read()); 
  }
  Serial.println("-");

  Serial.println("--- end");
#endif







  //  delay(5000);
  //  gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
  //  delay(1000);
  //  gsm.println("AT+CIICR");
  //  delay(1000);
  //  gsm.println("AT+CIFSR");



#ifdef debug_conection
  Serial.println("Iniciando conexão com a internet ...");
  //  delay(5000);

  Serial.print("\nData recebida: ");
  Serial.println(gsm.available());

  while(gsm.available() > 0) {
    Serial.print((char)gsm.read());
  }

  Serial.println("--- end");
#endif
}


























//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO
//          CONFIGURAÇÃO DO MÓDULO

void power_on() {
  digitalWrite(on_module_pin,HIGH);
  delay(1200);
  digitalWrite(on_module_pin,LOW);
  delay(1000);
}

//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DE REQUISIÇÃO

int request_state = 0;
long old_module_request_time = 0;
boolean response_analised = true;    // APENAS ENVIA REQUEST SE A RESPOSTA TIVER SIDO ANALISADA.


void send_request_module() {
  switch (request_state) {
  case 0:
    gsm.println("AT");
    Serial.print("P: Verificando status do modulo ::\n");

    finish_request_module();
    break;
  case 1:
    gsm.println("AT+CIPSHUT");
    Serial.print("P: Desconectando modulo ::\n");
    break;
  case 2:
    gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
    Serial.print("P: Configurando modulo ::\n");

    finish_request_module();

    break;
  case 3:
    gsm.println("AT+CIICR");
    Serial.print("P: Iniciando processo de conexao no modulo ::\n");

    finish_request_module();

    break;
    //  case 3:  
    //    gsm.println("AT+CIFSR");
    //    Serial.print("P: Requisitando IP ::\n");

    //    finish_request_module();
    //    break;
  case 4:
//    gsm.println("AT+CIPSTART=\"TCP\",\"www.meioamb.eng.br\",80");
//    gsm.println("AT+CIPSTART=\"TCP\",\"www.monitoramento.br.hos.tf\",80");
// JEOVANE OFF    gsm.println("AT+CIPSTART=\"TCP\",\"www.projetoruidos.com.br\",80");
      gsm.println("AT+CIPSTART=\"TCP\",\"www.ruidoterminal.com.br\",80");
    delay(10);
    Serial.print("P: Requisitando conexao com o servidor ::\n");

    finish_request_module();
    break;
  case 5:
    gsm.println("AT+CIPSEND");
    Serial.print("P: Iniciando envio de dados ::\n");

    finish_request_module();

    break;
  case 6:
    gsm.print("GET /insert.php?token=2526272625&leitura=");
    gsm.print(getData());                        //       OBTEM OS DADOS A SEREM ENVIADOS ...
    gsm.println(" HTTP/1.0");
    gsm.println("Host: www.projetoruidos.com.br");
    gsm.print("User-agent: Arduino");
    gsm.println("\n");
    delay(100);
    gsm.write(0x1a);
    gsm.println("\0");

    Serial.print("Enviando dados ::\n");

    finish_request_module();
    break;
  }
}



//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE
//          CONFIGURAÇÃO DO MÓDULO     ---    PROCESSAMENTO DO RESPONSE

int response_state = 0;
long old_response_analised_time = 0;  //  TEMPO DA ULTIMA RESPOSTA ANALISADA (CONFIRMADA).

int count = 0;
void response_analiser() {
  count++;
  Serial.print(" SL=");
  Serial.println(count);
  if (count >= 8) {
    count = 0;
    request_state = 0;
    power_on();
    Serial.print("P: Security Loop ativado :: Reiniciando\n");
  }

  String received = "";
  if ((gsm.available()>0)) {
    received = "";

    while(gsm.available()>0) {
      received += (char)gsm.read();
      delay(50);
    }
    Serial.print("Recebido: ");
    Serial.println(received);
    int response;
    switch (response_state) {
    case 0:
      response = response_analise(received,"OK","NORMAL POWER DOWN");
      if (response == 1) {
        Serial.print("M: Modulo ligado e respondendo ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Modulo desligado ou nao respondendo ::\n");
      }
      break;
    case 1:
      response = response_analise(received, "SHUT OK","ERROR");
      if (response == 1) {
        Serial.print("M: GPRS desligado ::\n"); 
      } 
      else if (response == 0) {
        Serial.print("M: falha em desligar o GRPS ::\n");
      }
      break;
    case 2:
      response = response_analise(received,"OK","ERROR");
      if (response == 1) {
        Serial.print("M: Configuracao aceita ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Configuracao negada ::\n");
      }
      break;
    case 3:
      response = response_analise(received,"OK","ERROR");
      if (response == 1) {
        Serial.print("M: Requisicao de conexao aceita ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Requisicao de conexao negada ::\n");
      }
      break;
      //    case 3:
      //      response = response_analise(received,"null","ERROR");
      //      if (response == 1) {
      //        Serial.print("M: IP Confirmado com sucesso ::\n");
      //      } 
      //      else if (response == 0) {
      //        Serial.print("M: IP Nao Confirmado ::\n");
      //      }
      //      break;
    case 4:
      response = response_analise(received,"CONNECT OK","ERROR");
      if (response == 1) {
        Serial.print("M: Conexao estabelecida com a internet ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Conexao recusada com o servidor ::\n");
      }
      break;
    case 5:
      response = response_analise(received,">","ERROR");
      if (response == 1) {
        Serial.print("M: Modulo aceitando envio de dados ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Envio de dados negado ::\n");
      }
      break;
    case 6:
      response = response_analise(received,"CLOSE","ERROR");
      if (response == 1) {
        Serial.print("M: Dados enviados com sucesso, reiniciando envio. ::\n");
      } 
      else if (response == 0) {
        Serial.print("M: Dados nao enviados. ::\n");
      }
      break;
    }
  }
}

int response_analise(String received, String TRUE, String FALSE) {
  count = 0;

  if (received.indexOf(FALSE) > -1) {
    back_request();
    return 0;
  }
  else if ((TRUE == "null") || (received.indexOf(TRUE) > -1)) {
    next_request();
    return 1;
  } 
  else if ((FALSE == "null")) {
    back_request();
    return 0;    
  }
}


// VÍNCULO ENTRE O REQUEST E O RESPONSE ::

void finish_request_module() {
  response_state = request_state;
  response_analised = false;
  old_module_request_time = millis();
  Serial.print("request_state=");
  Serial.print(request_state);
  //  Serial.print("Aguardando analise ::\n");
}

void back_request() {
  if (request_state > 0) {
    request_state--;
  }
  response_analised = true;
}
void next_request() {
  if (request_state < 6){
    request_state++;
  } 
  else if (request_state == 6) {
    request_state = 4;
  }
  response_analised = true;
}






//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR
//            CONFIGURAÇÃO DA LEITURA DO SENSOR

int sensor_data = 0;
void read_sensor() {
  sensor_data = analogRead(A0);
}
float getData() {
  return sensor_data * (5.0 / 1023.0) * 100;
}























/*
void loop() {

  read_sensor();

  response_analiser();
  send_request_module();

  delay(1500);

  if (request_state == 6) {
    delay(5000);
  }
}
*/
boolean continua = true;
void loop() {
  gsm.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
  Serial.println("AT+CSTT=\"internet.wind\",\"\",\"\"");
  delay(1000);
  gsm.println("AT+CIICR");
  Serial.println("AT+CIICR");
  delay(1000);
  gsm.println("AT+CIFSR");
  Serial.println("AT+CIFSR");
  delay(1000);
  continua = true;
  while (continua) {
    read_sensor();
    gsm.println("AT+CIPSTART=\"TCP\",\"www.projetoruidos.com.br\",80");
    Serial.println("AT+CIPSTART=\"TCP\",\"www.projetoruidos.com.br\",80");
    delay(1000);
    gsm.println("AT+CIPSEND");
    Serial.println("AT+CIPSEND");
    delay(1000);
    gsm.print("GET /insert.php?token=2526272625&leitura=");
    float temp = getData();                       //       OBTEM OS DADOS A SEREM ENVIADOS ...
    gsm.print(temp); 
    gsm.println(" HTTP/1.0");
    gsm.println("Host: www.projetoruidos.com.br");
    gsm.print("User-agent: Arduino");
    gsm.println("\n");
    delay(100);
    gsm.write(0x1a);
    gsm.println("\0");
    Serial.print("[[temp:");
    Serial.print(temp);
    Serial.println("]]");
    Serial.println("Enviando dados ...");
    delay(5000);
    String received = "";
    if ((gsm.available()>0)) {
      received = "";

      while(gsm.available()>0) {
        received += (char)gsm.read();
        delay(50);
      }
      Serial.print("Recebido: ");
      Serial.println(received);
      if (received.indexOf("ERROR") > -1) {
        continua = false;
      }
    }
  }
  gsm.println("AT+CIPSHUT");
  Serial.println("AT+CIPSHUT");
  delay(1000);
}











