void setup() /*----( SETUP: RUNS ONCE )----*/
{
Serial.begin(9600); // Velocidade de transmissão

lcd.begin(20,4); // nesse exemplo utilizo um lcd 20x4........backlight

// ------- Quick 3 blinks of backlight -------------
for(int i = 0; i< 3; i++)
{
lcd.backlight();
delay(250);
lcd.noBacklight();
delay(250);
}
lcd.backlight(); // finish with backlight on 

//-------- Write characters on the display ------------------
// NOTE: Cursor Position: Lines and Characters start at 0 
lcd.setCursor(3,0); //Start at character 4 on line 0
lcd.print("ENG.LUCAS");
delay(1000);
lcd.setCursor(2,1);
lcd.print("ENG.ELETRICA");
delay(1000); 

lcd.setCursor(0,2);
lcd.print("L.C.D");
lcd.setCursor(0,3);
delay(2000); 
lcd.print("lucaswmsilva@gmail");
delay(8000);
lcd.clear();
// Wait and then tell user they can start the Serial Monitor and type in characters to
// Display. (Set Serial Monitor option to "No Line Ending")
lcd.setCursor(0,0); //Start at character 0 on line 0
lcd.print("Inic. Monitor Serial");
lcd.setCursor(0,1);
lcd.print("ESCREVA"); 

}/*--(end setup )---*/

void loop() /*----( LOOP: RUNS CONSTANTLY )----*/
{
{
// when characters arrive over the serial port...
if (Serial.available()) {
// wait a bit for the entire message to arrive
delay(100);
// clear the screen
lcd.clear();
// read all the available characters
while (Serial.available() > 0) {
// display each character to the LCD
lcd.write(Serial.read());
}
}
}

}/* --(end main loop )-- */

/* ( THE END ) */
