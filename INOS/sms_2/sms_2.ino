#include <SoftwareSerial.h>

int8_t answer;
int onModulePin = 8; // PINO PARA LIGAR O MÓDULO.
char aux_string[50] = "Digite aqui a mensagem a ser enviada.";
char phone_number[]="00000000"; // Coloque aqui o número de celular para enviar a men;
SoftwareSerial gsm(2,3);

void setup(){


  pinMode(onModulePin, OUTPUT);
  Serial.begin(9600);    

  Serial.println("Iniciando comunicação com GSM");
  gsm.begin(2400);

  Serial.println("Ligando Shield...");
  power_on();

  delay(3000);

  Serial.println(sendSMS());
}


void loop(){

}

//FUNÇÃO PARA LIGAR O SHIELD GSM
void power_on(){
    digitalWrite(onModulePin,HIGH);
    delay(1200);
    digitalWrite(onModulePin,LOW);
    delay(1000);
}

char* sendSMS() {
  delay(500);
  gsm.println("AT+CMGF=1");
  delay(500);
  gsm.print("AT+CMGS=");
  gsm.print((char)byte(34));
  gsm.print(phone_number);
  gsm.print((char)byte(34));
  gsm.println();
  delay(500);
  gsm.print(aux_string);
  gsm.print((char)byte(26));

  Serial.print("Lendo GSM: "); 
  while(gsm.available() > 0) Serial.print((char)gsm.read());    // Clean the input buffer
  Serial.print("-------------- FIM Leitura GSM: "); 
  Serial.print("\n");
  Serial.print("SMS Enviada .. Aguardando resposta: \n");

  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;
  boolean wait = true;
  int timeout = 20000;
  x=0;

  do {
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if(gsm.available() != 0){
      response[x] = (char)gsm.read();
      x++;
      wait = false;
      // check if the desired answer is in the response of the module
    }
    // Waits for the asnwer with time out
  } 
  while((wait == true) || ((millis() - previous) < timeout));    

  return response;
}

