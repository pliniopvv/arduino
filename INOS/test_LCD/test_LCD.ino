
#include <LiquidCrystal.h>


LiquidCrystal Lcd(12,11,5,4,3,2);

void setup() {
  // put your setup code here, to run once:
   Lcd.begin(16,2);
   Lcd.print("Curso Arduino Básico");
}

void loop() {
  // put your main code here, to run repeatedly: 
  Lcd.setCursor(0,1);
  Lcd.print(millis()/1000);
}
