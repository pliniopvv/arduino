#include "SIM900.h"
#include <SoftwareSerial.h>

//If you want to use the Arduino functions to manage SMS, uncomment the lines below.
#include "sms.h"
SMSGSM sms;

boolean started=false;
char smsbuffer[160];
char n[20];

void setup()
{
  Serial.begin(2400);
  if (gsm.begin(2400)){
    Serial.println("\nstatus=READY");
    started=true;  
  }
}

void loop()
{
  delay(500);
  gsm.println("AT+CMGF=1");
  Serial.println("AT+CMGF=1");
  delay(500);
  gsm.print("AT+CMGS=");
  gsm.write(byte(34));
  gsm.print("0414588073105"); // Leonardo
  // gsm.print("0413499288909"); // Denise
  gsm.write(byte(34));
  gsm.println();
  Serial.print("AT+CMGS=");
  Serial.write(byte(34));
  Serial.print("0413499288909");
  Serial.write(byte(34));
  Serial.println();
  delay(500);
  gsm.print("Momo, tiamu! '-' ...");
  gsm.write(byte(26));
  Serial.print("Momo, tiamu! '-' ...");
  Serial.write(byte(26));

  Serial.println("Lendo mensagens.");
while (1)
{
    //Read if there are messages on SIM card and print them.
    if(gsm.readSMS(smsbuffer, 160, n, 20)) {
      Serial.println(n);
      Serial.println(smsbuffer);
    }
    if (Serial.available()>0) {
      Serial.println("Lendo serial");
      String mensagem = "";
      while (Serial.available()>0) {
          char inChar = (char)Serial.read();          
          mensagem += inChar;
      }
      Serial.println(mensagem);
    }
    delay(1000);
};

}
