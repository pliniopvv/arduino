/*
 *  Description: This example shows how to send a SMS to a desired phone.
 *  This example only shows the AT commands (and the answers of the module) used
 *  to send the SMS. For more information about the AT commands, refer to the AT 
 *  command manual.
 *
 *  Copyright (C) 2013 Libelium Comunicaciones Distribuidas S.L.
 *  http://www.libelium.com
 *
 *  This program is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or 
 *  (at your option) any later version. 
 *  
 *  This program is distributed in the hope that it will be useful, 
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *  GNU General Public License for more details. 
 *  
 *  You should have received a copy of the GNU General Public License 
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *
 *  Version 0.2
 *  Author: Alejandro Gallego 
 */

#include <SoftwareSerial.h>

int8_t answer;
int onModulePin = 8; // PINO PARA LIGAR O MÓDULO.
char aux_string[30] = "FONÇONA MOOOO!";
//char phone_number[]="99288909"; // tel Denise
char phone_number[]="92054014"; // tel Plinio;
SoftwareSerial gsm(2,3);

void setup(){


  pinMode(onModulePin, OUTPUT);
  Serial.begin(9600);    

  Serial.println("Iniciando comunicação com GSM");
  gsm.begin(2400);

  Serial.println("Ligando Shield...");
  power_on();

  delay(3000);



  //Serial.println(sendSMS());
  Serial.println(sendATcommand("AT+CSQ","OK",2000));



  // sets the PIN code
  // sendATcommand("AT+CPIN=****", "OK", 2000);
  // delay(3000);

  //  Serial.println("Connecting to the network...");
  //  while( (sendATcommand("AT+CREG?", "+CREG: 0,1", 500) || sendATcommand("AT+CREG?", "+CREG: 0,5", 500)) == 0 );

  /*

   Serial.print("Setting SMS mode...");
   sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text
   Serial.println("Sending SMS");
   
   Serial.println(sprintf(aux_string,"AT+CMGS=\"%s\"", phone_number));
   answer = sendATcommand(aux_string, ">", 2000);    // send the SMS number
   if (answer == 1)
   {
   Serial.println("Test-Arduino-Hello World");
   Serial.write(0x1A);
   answer = sendATcommand("", "OK", 20000);
   if (answer == 1)
   {
   Serial.print("Sent ");    
   }
   else
   {
   Serial.print("error ");
   }
   }
   else
   {
   Serial.print("error ");
   Serial.println(answer, DEC);
   }
   */
}


void loop(){

}

void power_on(){

  uint8_t answer=0;

  // checks if the module is started
  answer = sendATcommand("AT", "OK", 2000);

  if (answer == 0)
  {
    // power on pulse
    digitalWrite(onModulePin,HIGH);
    delay(1200);
    digitalWrite(onModulePin,LOW);
    delay(1000);

    // waits for an answer from the module
    while(answer == 0){     // Send AT every two seconds and wait for the answer
      answer = sendATcommand("AT", "OK", 2000);
    }
  }

}

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){

  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialice the string


  delay(100);
  Serial.print("Lendo GSM: "); 
  while(gsm.available() > 0) Serial.print((char)gsm.read());    // Clean the input buffer
  Serial.print("-------------- FIM Leitura GSM: "); 

  Serial.print("\n");
  Serial.print("Enviando Comando: ");
  Serial.println(ATcommand);    
  gsm.println(ATcommand);    // Send the AT command 


    x = 0;
  previous = millis();

  // this loop waits for the answer
  do{
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if(gsm.available() != 0){
      response[x] = (char)gsm.read();
      x++;
      // check if the desired answer is in the response of the module
      if (strstr(response, expected_answer) != NULL)    
      {
        answer = 1;
      }
    }
    // Waits for the asnwer with time out
  }
  while((answer == 0) && ((millis() - previous) < timeout));    
  Serial.print("--- Response: ");
  Serial.println(response);
  Serial.println("------------ Fim RESPONSE");
  return answer;
}

char* sendSMS() {
  delay(500);
  gsm.println("AT+CMGF=1");
  delay(500);
  gsm.print("AT+CMGS=");
  gsm.print((char)byte(34));
  gsm.print("92054014");
  gsm.print((char)byte(34));
  gsm.println();
  delay(500);
  gsm.print("Mensagem de pobre!");
  gsm.print((char)byte(26));

  Serial.print("Lendo GSM: "); 
  while(gsm.available() > 0) Serial.print((char)gsm.read());    // Clean the input buffer
  Serial.print("-------------- FIM Leitura GSM: "); 
  Serial.print("\n");
  Serial.print("SMS Enviada .. Aguardando resposta: \n");

  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;
  boolean wait = true;
  int timeout = 20000;
  x=0;

  do {
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if(gsm.available() != 0){
      response[x] = (char)gsm.read();
      x++;
      wait = false;
      // check if the desired answer is in the response of the module
    }
    // Waits for the asnwer with time out
  } 
  while((wait == true) || ((millis() - previous) < timeout));    

  return response;
}

