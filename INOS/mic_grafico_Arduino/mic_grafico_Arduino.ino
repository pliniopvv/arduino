// Importa a biblioteca de comunicação serial 
import processing.serial.*; 
// Cria um objeto para comunicação 
Serial myPort; 
float xPos = 1;
float yPos0 = 1;
float yPos1 = 1;
float dt=1;
int i=0;
float inY0, inY1;
void setup () {
  // Define o tamanho da janela 
  size(800, 300);
  //Conecta-se com o primeiro dispositivo da lista 
  myPort = new Serial(this, Serial.list()[0], 9600); 
  // Espera receber o primeiro sinal da usb 
  myPort.bufferUntil('\n'); 
  // Define a cor de fundo da janela 
  background(255); 
} 
void draw () { 
  // Nada será feito aqui 
} 
void serialEvent (Serial myPort) { 
  // Pega a primeira linha recebida na porta serial 
  String inString = myPort.readStringUntil('\n'); 
  // Se a linha não estiver em branco 
  if (inString != null) { 
    // remove os espaços em branco 
    inString = trim(inString); 
    // Divide a string num vetor separado por ; 
    String[] vector = split(inString,';'); 
    // converte para int e muda os limites (map): 
    inY0 = map(float(vector[0]), 0, 1023, 0, 350); 
    inY1 = map(float(vector[1]), 0, 1023, 0, 350); 
    // Desenha as linhas: 
    stroke(255,0,0); 
    line(xPos,yPos0, xPos+dt, height - inY0); 
    yPos0=height-inY0; 
    stroke(0,0,0); 
    line(xPos,yPos1, xPos+dt, height - inY1); 
    yPos1=height-inY1; 
    // Se chegar no final da janela, volta para o início 
    if (xPos >= width) { 
      xPos = 0; 
      background(255); 
    } 
    else { 
      xPos+=dt; 
    } 
  } 
} 

