/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor.
 Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

volatile int aV[5] = {
  0,0,0,0,0};
int maxV = 5;
volatile int sensorValue = 0;
// the loop routine runs over and over again forever:
void loop() {


  int sensor = analogRead(A0);
  if (sensor > 500) {
    for (int i = 0;i < maxV-1;i++){ // troca os valores de posiçao nas variaveis
      aV[i] = aV[i+1];
    }
    aV[maxV-1] =  analogRead(A0); // adiciona o ultimo valor lido

    sensorValue = 0;
    for (int i = 0; i <= maxV-1;i++) { // Soma os valores para a media
      sensorValue += aV[i];
      //    Serial.println(aV[i]);
    }
    sensorValue = sensorValue/maxV; // Calcula a media

    // print out the value you read:
    //  if (sensorValue > 512) {
    Serial.print(sensorValue);
    Serial.println(";500");
    delay(10);        // delay in between reads for stability
    //  }
  }
}



