// Sweep
// by BARRAGAN <http://barraganstudio.com>
// This example code is in the public domain.


#include <Servo.h>

Servo myservo;

int pos = 0;

void setup()
{
  Serial.begin(9600);
  myservo.attach(3);
}


void loop()
{
  if (Serial.available()) {
    pos = (int) Serial.parseInt();
    Serial.println(pos);
    if (pos > 180) {
      pos = 180;
    }
    myservo.write(pos);
  }
  delay(1000);
}
