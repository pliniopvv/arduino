#define debug

const int pinPower = 8;

// DEFINITIONS

const int buffGSMSize = 50;

#include <SoftwareSerial.h>

SoftwareSerial gsm(2,3);

void setup() {

  Serial.begin(9600);
  gsm.begin(2400);

#ifdef debug
  Serial.println("");
#endif

#ifdef debug
  Serial.println("Serial e SoftwareSerial iniciados ...");
#endif

#ifdef debug
  Serial.println("Inicializando pinos ...");
#endif

  pinMode(pinPower, OUTPUT);

}

boolean power_on() {

  digitalWrite(pinPower, HIGH);
  delay(1200);
  digitalWrite(pinPower, LOW);

  gsm.println("AT");
  delay(1200);

  while (true) {

  }

}

String readSERIAL() {
}

boolean GSMtoSERIAL() {
  char buff[buffGSMSize];

  if (gsm.available() > 50) {
    while (gsm.available() > 50) {
      buff = readGSM();
      Serial.print(buff);
    }
  } 
  if (gsm.available() > 0) {
    buff = readGSM();
    Serial.print(buff);
  }
  return true;
}

char readGSM() {
  char buff[buffGSMSize];
  if (gsm.available() > 50) {
    int cont = 0;
    while (cont <= 50) {

      buff[cont] = gsm.read();

      cont++; // +1 NO CONTADOR;
    }
  }
  return buff; 
}

void loop() {

}


