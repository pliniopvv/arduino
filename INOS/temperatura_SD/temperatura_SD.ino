/*
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** CLK - pin 13
 ** MISO - pin 12
 ** CS - pin 4
 */

#include <SD.h>
//#include "floatToString.h"

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 4;
int analogPin = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(10, OUTPUT);
  if (!SD.begin(chipSelect)) {
    Serial.println("Falha em iniciar");
    return;
  } 
  else {
    Serial.println("Cartao inicializado.");
  }
  checkFormatFile();
}

void loop()
{
  int sensor = analogRead(analogPin);
  float temp = sensor*(5.0/1023.0)*100.0;
  
  char data[5];
  dtostrf(temp,2,2,data);

  Serial.print("Registrando : ");
  Serial.println(data);
  
  registrarTemperatura(data);
  delay(1000);
}

void registrarTemperatura(String data)
{
  File dataFile = SD.open("temp.csv", FILE_WRITE);
  if (!SD.exists("temp.csv")) {
    Serial.println("O arquivo nao foi criado.");
    checkFormatFile();
  }
  if (dataFile) {
    dataFile.println(String(millis()) + ";" + data);
    dataFile.close();
  }
}
boolean checkFormatFile()
{
  if (!SD.exists("temp.csv")) {
    Serial.println("Criando arquivo.");
    SD.open("temp.csv", FILE_WRITE);
    if (SD.exists("temp.csv")) {
      Serial.println("Arquvo criado.");
      File dataFile = SD.open("temp.csv", FILE_WRITE);
      dataFile.println(String("MiliSegundos;Dados"));
      dataFile.close();
    } 
    else {
      while (true) {
        Serial.println("Falha em criar o arquivo.");
        delay(1000);
      }
    }
  }
}

