/*
Projeto Arduino contador binário com saída em 8 leds que formam 1 byte.
 Por Jota
 ----------------------------------------
 --=<| www.ComoFazerAsCoisas.com.br |>=--
 ----------------------------------------
 */

#define pinData 2 //pino do arduino ligado ao pinData do 74HC595
#define pinLatch 3 //pino do arduino ligado ao pinLatch do 74HC595
#define pinClock 4 //pino do arduino ligado ao pinClock do 74HC595
#define pinClear 5 //pino do arduino ligado ao clear do 74HC595

void setup() {
  //definindo os pinos como de saída
  pinMode(pinClock,OUTPUT);
  pinMode(pinLatch,OUTPUT);
  pinMode(pinData,OUTPUT);
  Serial.begin(9600);
}


#define maxBits 16
void loop() {

  /*
   clearBits();
   sendBit(HIGH);
   Serial.println("Enviando...");
   delay(1000);
   */
  int bites[maxBits] = {
    1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1    };

  Serial.println("Enviando bits ...");
  clearBits();
  Serial.println("Enviando dados ...");
  EnviaByte(bites);
  Serial.print("\nFim ...");
  while(1);
}

void sendBit(int b) {
  digitalWrite(pinLatch, LOW); // permite gravar os dados.
  delay(1);
  digitalWrite(pinClock, LOW); // Permite envio de bit;
  delay(1);
  digitalWrite(pinData, b); // Envia o bit;
  delay(1);
  digitalWrite(pinClock, HIGH); // Registra o bit;
  delay(1);
  digitalWrite(pinLatch, HIGH); // Finaliza transmicao dos dados.
  delay(100);
  //digitalWrite(pinLatch, LOW);
}

void EnviaByte(int bytes[]) {
  digitalWrite(pinLatch, LOW); // permite gravar os dados.
  for (int i = 0; i < maxBits;i++) {
    digitalWrite(pinClock, LOW); // Permite envio de bit;
    delay(1);
    digitalWrite(pinData, bytes[i]); // Envia o bit;
    Serial.print(bytes[i]);
    digitalWrite(pinClock, HIGH); // Registra o bit;
    delay(1);
  }
  digitalWrite(pinLatch, HIGH); // Finaliza transmicao dos dados.
  delay(100);
  //  digitalWrite(pinLatch, LOW);
}
void clearBits() {
  digitalWrite(pinClear, HIGH);
  delay(1);
  digitalWrite(pinClear, LOW);
  delay(100);
}










