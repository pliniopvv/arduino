/*
  SD card datalogger
 
 This example shows how to log data from three analog sensors 
 to an SD card using the SD library.
 	
 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created  24 Nov 2010
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */

#include <SD.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <DS1307.h>

#define WIRE_SENSORES 8
#define WIRE_SD 7
int led = 3;

DS1307 relogio(A4, A5);

OneWire oneWire(WIRE_SENSORES);

DallasTemperature sensores(&oneWire);
DeviceAddress sensor1;
DeviceAddress sensor2;
DeviceAddress sensor3;
DeviceAddress sensor4;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 7;

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(7, OUTPUT);

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");

  //As linhas abaixo setam a data e hora do modulo
  //e podem ser comentada apos a primeira utilizacao
  //  relogio.setDOW(MONDAY);      //Define o dia da semana
  //  relogio.setTime(17, 34, 50);     //Define o horario
  //  relogio.setDate(9, 5, 2016);   //Define o dia, mes e ano

  sensores.begin();
  if (!sensores.getAddress(sensor1, 0))
    Serial.println("Sensor 01 nao encontrado !");
  if (!sensores.getAddress(sensor2, 1))
    Serial.println("Sensor 02 nao encontrado !"); 
  if (!sensores.getAddress(sensor3, 2))
    Serial.println("Sensores 03 nao encontrado !"); 
  if (!sensores.getAddress(sensor4, 3))
    Serial.println("Sensores 04 nao encontrado !"); 
}

void loop()
{
  // make a string for assembling the data to log:
  String dataString = "";

  sensores.requestTemperatures();
  //Sensor
  dataString += hora_agora();
  dataString += ";";
  dataString += pega_cod(sensor1); // XXXX fazer código
  dataString += ";";
  float temp = sensores.getTempC(sensor1);
  char t[8];
  dtostrf(temp,4,4,t);
  dataString += t;
  dataString += ";";
  dataString += pega_cod(sensor2); // XXXX fazer código
  dataString += ";";
  temp = sensores.getTempC(sensor2);
  t[8];
  dtostrf(temp,4,4,t);
  dataString += t;
  dataString += ";";
  dataString += pega_cod(sensor3); // XXXX fazer código
  dataString += ";";
  temp = sensores.getTempC(sensor3);
  t[8];
  dtostrf(temp,4,4,t);
  dataString += t;
  dataString += ";";
  dataString += pega_cod(sensor4); // XXXX fazer código
  dataString += ";";
  temp = sensores.getTempC(sensor4);
  t[8];
  dtostrf(temp,4,4,t);
  dataString += t;

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
    alerta_piscar();
  }  
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
    alerta_fade();
  }

  delay(1000);
}

String pega_cod(DeviceAddress deviceAddress) {
  String data = "";
  for (uint8_t i = 0; i < 8; i++)  {
    // Adiciona zeros se necessário
    if (deviceAddress[i] < 16) { 
      data += "0"; 
    }
    data += String(deviceAddress[i], HEX);
  }
  return data;
}

String hora_agora() {
  String data = "";
  data += relogio.getDateStr(FORMAT_SHORT);
  data += " ";
  data += relogio.getTimeStr();
  return data;
}

void alerta_piscar() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(500);
}

void alerta_fade() {
  for (int i = 0; i<256;i++) {
    analogWrite(led, i);    
    delay(1);
  }
  for (int i = 255;i > 0;i--) {
    analogWrite(led, i);
    delay(1);
  }
  analogWrite(led, 0);
}





