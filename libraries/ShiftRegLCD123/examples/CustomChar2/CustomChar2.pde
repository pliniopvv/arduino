// Example creating custom characters 0-7 on a HD44700 compatible LCD
// using 1-wire connection to the LCD

#include <ShiftRegLCD123.h>

const byte lcdPin  = 10;    // LCD stream from Arduino pin 10

// For 1- and 2- wire, SRLCD123 and SRLCDOLD are identical
ShiftRegLCD123 srlcd(lcdPin, SRLCD123);

void setup()
{
  // Initialize LCD to 20 columns, 2 lines.
  srlcd.begin(20,2);
  
  // Turn backlight on (if used)
  srlcd.backlightOn();

  // Some very simple characters, could be used for a bar-graph
  uint8_t customchar[][8] ={ { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F },
                         { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F },
                         { 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x1F },
                         { 0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x1F }, 
                         { 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F },
                         { 0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F },
                         { 0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F },
                         { 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F } };

  // Program the characters into the LCD
  for (uint8_t c = 0;c<8;c++) {
      srlcd.createChar(c, customchar[c]);
  }

  // Print the custom characters to the LCDs 1. line
  for (uint8_t i=0;i<8;i++) srlcd.write(i);
  // Line and columns are zero-based (first line = 0)
  srlcd.setCursor(0,1);
  // Print another message on line 2
  srlcd.print("CUSTOM CHARACTERS");
  // Print them out backwards on line 1 again
  srlcd.setCursor(9,0);
  for (uint8_t i=8;i>0;i--) srlcd.write(i-1);
}

void loop()
{
}

